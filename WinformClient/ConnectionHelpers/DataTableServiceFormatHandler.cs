﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace WinformClient.ConnectionHelpers
{
    public static class DataTableServiceFormatHandler
    {
        public static AIMService.DataTableServiceFormat FromDataTable(DataTable data)
        {
            AIMService.DataTableServiceFormat convertedData = new AIMService.DataTableServiceFormat();
            List<string> headers = new List<string>();
            List<object> values = new List<object>();
            foreach (DataColumn column in data.Columns)
            {
                headers.Add(column.ColumnName);
            }
            foreach (DataRow row in data.Rows)
            {
                foreach (DataColumn column in data.Columns)
                {
                    values.Add(row[column]);
                }
            }
            convertedData.Headers = headers.ToArray();
            convertedData.Values = values.ToArray();
            return convertedData;
        }

        public static DataTable ToDataTable(AIMService.DataTableServiceFormat data)
        {
            DataTable convertedData = new DataTable();
            List<string> headers = new List<string>(data.Headers);
            List<object> values = new List<object>(data.Values);
            foreach (string columnName in headers)
            {
                convertedData.Columns.Add(columnName);
            }
            int columnCount = data.Headers.Count();
            if (columnCount > 0)
            {
                int rowCount = data.Values.Count() / columnCount;
                for (int i = 0; i < rowCount; ++i)
                {
                    List<object> rowValues = new List<object>();
                    for (int j = 0; j < columnCount; ++j)
                    {
                        rowValues.Add(values[i * columnCount + j]);
                    }
                    convertedData.Rows.Add(rowValues.ToArray());
                }
            }
            return convertedData;
        }
    }
}