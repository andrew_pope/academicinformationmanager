﻿using System;
using System.Data;
using System.ServiceModel;
using System.Windows.Forms;
using WinformClient.ConnectionHelpers;

namespace WinformClient.GUI
{
    public partial class ExampleForm : Form
    {

        AIMService.AIMServiceClient m_service = new AIMService.AIMServiceClient();

        public ExampleForm()
        {
            InitializeComponent();
            DataTable tableNames = new DataTable();
            DataTable displayFields = new DataTable();
            DataTable filter = new DataTable();
            DataTable order = new DataTable();
            tableNames.Columns.Add("Account");
            displayFields.Columns.Add("Account");
            DataTable data = DataTableServiceFormatHandler.ToDataTable(
                m_service.Filter(
                    DataTableServiceFormatHandler.FromDataTable(tableNames),
                    DataTableServiceFormatHandler.FromDataTable(displayFields),
                    DataTableServiceFormatHandler.FromDataTable(filter),
                    DataTableServiceFormatHandler.FromDataTable(order)));
            dgv.DataSource = data;
            dgv.Columns["AccountID"].Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            table.Columns.Add("TableName");
            table.Columns.Add("AccountUsername", typeof(string));
            table.Columns.Add("AccountPassword", typeof(string));
            table.Columns.Add("AccountType", typeof(string));
            table.Columns.Add("AccountName", typeof(string));
            table.Columns.Add("AccountEmail", typeof(string));
            table.Columns.Add("AccountApproved", typeof(bool));
            DataRow row = table.NewRow();
            row["TableName"] = "Account";
            row["AccountUsername"] = "fsdsf";
            row["AccountPassword"] = "fsdfsd";
            row["AccountType"] = "Student";
            row["AccountName"] = "fsdfds";
            row["AccountEmail"] = "fsdfds";
            row["AccountApproved"] = true;
            table.Rows.Add(row);
            try
            {
                m_service.Add(DataTableServiceFormatHandler.FromDataTable(table));
            }
            catch (FaultException<string> ex)
            {
                MessageBox.Show(ex.Detail);
            }
            DataTable tableNames = new DataTable();
            DataTable displayFields = new DataTable();
            DataTable filter = new DataTable();
            DataTable order = new DataTable();
            tableNames.Columns.Add("Account");
            displayFields.Columns.Add("Account");
            DataTable data = DataTableServiceFormatHandler.ToDataTable(
                m_service.Filter(
                    DataTableServiceFormatHandler.FromDataTable(tableNames),
                    DataTableServiceFormatHandler.FromDataTable(displayFields),
                    DataTableServiceFormatHandler.FromDataTable(filter),
                    DataTableServiceFormatHandler.FromDataTable(order)));
            dgv.DataSource = data;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            table.Columns.Add("TableName");
            table.Columns.Add("AccountID", typeof(string));
            table.Columns.Add("AccountUsername", typeof(string));
            table.Columns.Add("AccountPassword", typeof(string));
            table.Columns.Add("AccountType", typeof(string));
            table.Columns.Add("AccountName", typeof(string));
            table.Columns.Add("AccountEmail", typeof(string));
            table.Columns.Add("AccountApproved", typeof(bool));
            DataRow row = table.NewRow();
            row["TableName"] = "Account";
            row["AccountID"] = dgv.SelectedRows[0].Cells["AccountID"].Value;
            row["AccountUsername"] = "fsdsf";
            row["AccountPassword"] = "updated";
            row["AccountType"] = "Student";
            row["AccountName"] = "updated";
            row["AccountEmail"] = "fsdfds";
            row["AccountApproved"] = true;
            table.Rows.Add(row);
            m_service.Modify(DataTableServiceFormatHandler.FromDataTable(table));
            DataTable tableNames = new DataTable();
            DataTable displayFields = new DataTable();
            DataTable filter = new DataTable();
            DataTable order = new DataTable();
            tableNames.Columns.Add("Account");
            displayFields.Columns.Add("Account");
            DataTable data = DataTableServiceFormatHandler.ToDataTable(
                m_service.Filter(
                    DataTableServiceFormatHandler.FromDataTable(tableNames),
                    DataTableServiceFormatHandler.FromDataTable(displayFields),
                    DataTableServiceFormatHandler.FromDataTable(filter),
                    DataTableServiceFormatHandler.FromDataTable(order)));
            dgv.DataSource = data;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            table.Columns.Add("TableName");
            table.Columns.Add("AccountID", typeof(string));
            DataRow row = table.NewRow();
            row["TableName"] = "Account";
            row["AccountID"] = dgv.SelectedRows[0].Cells["AccountID"].Value;
            table.Rows.Add(row);
            m_service.Remove(DataTableServiceFormatHandler.FromDataTable(table));
            DataTable tableNames = new DataTable();
            DataTable displayFields = new DataTable();
            DataTable filter = new DataTable();
            DataTable order = new DataTable();
            tableNames.Columns.Add("Account");
            displayFields.Columns.Add("Account");
            DataTable data = DataTableServiceFormatHandler.ToDataTable(
                m_service.Filter(
                    DataTableServiceFormatHandler.FromDataTable(tableNames),
                    DataTableServiceFormatHandler.FromDataTable(displayFields),
                    DataTableServiceFormatHandler.FromDataTable(filter),
                    DataTableServiceFormatHandler.FromDataTable(order)));
            dgv.DataSource = data;
        }
    }
}
