﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WinformClient.AIMService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DataTableServiceFormat", Namespace="http://schemas.datacontract.org/2004/07/Service.Functional.ConnectionHelpers")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(string[]))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(object[]))]
    public partial class DataTableServiceFormat : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string[] HeadersField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private object[] ValuesField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string[] Headers {
            get {
                return this.HeadersField;
            }
            set {
                if ((object.ReferenceEquals(this.HeadersField, value) != true)) {
                    this.HeadersField = value;
                    this.RaisePropertyChanged("Headers");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public object[] Values {
            get {
                return this.ValuesField;
            }
            set {
                if ((object.ReferenceEquals(this.ValuesField, value) != true)) {
                    this.ValuesField = value;
                    this.RaisePropertyChanged("Values");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="AIMService.IAIMService")]
    public interface IAIMService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Add", ReplyAction="http://tempuri.org/IAIMService/AddResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://tempuri.org/IAIMService/AddStringFault", Name="string", Namespace="http://schemas.microsoft.com/2003/10/Serialization/")]
        void Add(WinformClient.AIMService.DataTableServiceFormat data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Add", ReplyAction="http://tempuri.org/IAIMService/AddResponse")]
        System.Threading.Tasks.Task AddAsync(WinformClient.AIMService.DataTableServiceFormat data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Modify", ReplyAction="http://tempuri.org/IAIMService/ModifyResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://tempuri.org/IAIMService/ModifyStringFault", Name="string", Namespace="http://schemas.microsoft.com/2003/10/Serialization/")]
        void Modify(WinformClient.AIMService.DataTableServiceFormat data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Modify", ReplyAction="http://tempuri.org/IAIMService/ModifyResponse")]
        System.Threading.Tasks.Task ModifyAsync(WinformClient.AIMService.DataTableServiceFormat data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Remove", ReplyAction="http://tempuri.org/IAIMService/RemoveResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://tempuri.org/IAIMService/RemoveStringFault", Name="string", Namespace="http://schemas.microsoft.com/2003/10/Serialization/")]
        void Remove(WinformClient.AIMService.DataTableServiceFormat data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Remove", ReplyAction="http://tempuri.org/IAIMService/RemoveResponse")]
        System.Threading.Tasks.Task RemoveAsync(WinformClient.AIMService.DataTableServiceFormat data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Filter", ReplyAction="http://tempuri.org/IAIMService/FilterResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://tempuri.org/IAIMService/FilterStringFault", Name="string", Namespace="http://schemas.microsoft.com/2003/10/Serialization/")]
        WinformClient.AIMService.DataTableServiceFormat Filter(WinformClient.AIMService.DataTableServiceFormat tableNames, WinformClient.AIMService.DataTableServiceFormat displayFields, WinformClient.AIMService.DataTableServiceFormat filters, WinformClient.AIMService.DataTableServiceFormat order);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAIMService/Filter", ReplyAction="http://tempuri.org/IAIMService/FilterResponse")]
        System.Threading.Tasks.Task<WinformClient.AIMService.DataTableServiceFormat> FilterAsync(WinformClient.AIMService.DataTableServiceFormat tableNames, WinformClient.AIMService.DataTableServiceFormat displayFields, WinformClient.AIMService.DataTableServiceFormat filters, WinformClient.AIMService.DataTableServiceFormat order);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAIMServiceChannel : WinformClient.AIMService.IAIMService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AIMServiceClient : System.ServiceModel.ClientBase<WinformClient.AIMService.IAIMService>, WinformClient.AIMService.IAIMService {
        
        public AIMServiceClient() {
        }
        
        public AIMServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AIMServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AIMServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AIMServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void Add(WinformClient.AIMService.DataTableServiceFormat data) {
            base.Channel.Add(data);
        }
        
        public System.Threading.Tasks.Task AddAsync(WinformClient.AIMService.DataTableServiceFormat data) {
            return base.Channel.AddAsync(data);
        }
        
        public void Modify(WinformClient.AIMService.DataTableServiceFormat data) {
            base.Channel.Modify(data);
        }
        
        public System.Threading.Tasks.Task ModifyAsync(WinformClient.AIMService.DataTableServiceFormat data) {
            return base.Channel.ModifyAsync(data);
        }
        
        public void Remove(WinformClient.AIMService.DataTableServiceFormat data) {
            base.Channel.Remove(data);
        }
        
        public System.Threading.Tasks.Task RemoveAsync(WinformClient.AIMService.DataTableServiceFormat data) {
            return base.Channel.RemoveAsync(data);
        }
        
        public WinformClient.AIMService.DataTableServiceFormat Filter(WinformClient.AIMService.DataTableServiceFormat tableNames, WinformClient.AIMService.DataTableServiceFormat displayFields, WinformClient.AIMService.DataTableServiceFormat filters, WinformClient.AIMService.DataTableServiceFormat order) {
            return base.Channel.Filter(tableNames, displayFields, filters, order);
        }
        
        public System.Threading.Tasks.Task<WinformClient.AIMService.DataTableServiceFormat> FilterAsync(WinformClient.AIMService.DataTableServiceFormat tableNames, WinformClient.AIMService.DataTableServiceFormat displayFields, WinformClient.AIMService.DataTableServiceFormat filters, WinformClient.AIMService.DataTableServiceFormat order) {
            return base.Channel.FilterAsync(tableNames, displayFields, filters, order);
        }
    }
}
