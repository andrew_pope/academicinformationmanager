﻿using Service.BusinessLogic;
using Service.Functional.ConnectionHelpers;
using Service.Functional.Exceptions;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Service
{
    /// <summary>
    /// Description:
    ///     AIMService is the class that implements the interface used by the web service
    /// Created by Andrei Popa on 07/04/2016
    ///     -Defined and implemented the AIMService class with te following method(s): Add, Modify, Remove, Filter
    /// </summary>
    public class AIMService : IAIMService
    {

        public void Add(DataTableServiceFormat data)
        {
            try
            {
                AIMController controller = new AIMController();
                controller.Add(DataTableServiceFormatHandler.ToDataTable(data));
            }
            catch (Exception e)
            {
                string exceptionReason;
                if (e is BaseException)
                    exceptionReason = ((BaseException)e).UserMessage;
                else
                    exceptionReason = "An internal system error has occurred! " + e.Message;
                throw new FaultException<string>(exceptionReason, new FaultReason(exceptionReason));
            }
        }

        public DataTableServiceFormat Filter(
            DataTableServiceFormat tableNames, 
            DataTableServiceFormat displayFields, 
            DataTableServiceFormat filters, 
            DataTableServiceFormat order)
        {
            try
            {
                AIMController controller = new AIMController();
                return DataTableServiceFormatHandler.FromDataTable(
                    controller.Filter(
                        DataTableServiceFormatHandler.ToDataTable(tableNames),
                        DataTableServiceFormatHandler.ToDataTable(displayFields),
                        DataTableServiceFormatHandler.ToDataTable(filters),
                        DataTableServiceFormatHandler.ToDataTable(order)));
            }
            catch (Exception e)
            {
                string exceptionReason;
                if (e is BaseException)
                    exceptionReason = ((BaseException)e).UserMessage;
                else
                    exceptionReason = "An internal system error has occurred! " + e.Message;
                throw new FaultException<string>(exceptionReason, new FaultReason(exceptionReason));
            }
        }

        public void Modify(DataTableServiceFormat data)
        {
            try
            {
                AIMController controller = new AIMController();
                controller.Modify(DataTableServiceFormatHandler.ToDataTable(data));
            }
            catch (Exception e)
            {
                string exceptionReason;
                if (e is BaseException)
                    exceptionReason = ((BaseException)e).UserMessage;
                else
                    exceptionReason = "An internal system error has occurred! " + e.Message;
                throw new FaultException<string>(exceptionReason, new FaultReason(exceptionReason));
            }
        }

        public void Remove(DataTableServiceFormat data)
        {
            try
            {
                AIMController controller = new AIMController();
                controller.Remove(DataTableServiceFormatHandler.ToDataTable(data));
            }
            catch (Exception e)
            {
                string exceptionReason;
                if (e is BaseException)
                    exceptionReason = ((BaseException)e).UserMessage;
                else
                    exceptionReason = "An internal system error has occurred! " + e.Message;
                throw new FaultException<string>(exceptionReason, new FaultReason(exceptionReason));
            }
        }
    }
}
