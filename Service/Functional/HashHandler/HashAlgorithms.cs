﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Service.Functional.HashAlgorithms
{
    /// <summary>
    /// Description:
    ///     HashAlgorithms is a collection class of hash algorithms.
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented HashAlgorithm class with SHA256Hash method
    /// </summary>
    public static class HashAlgorithms
    {
        /// <summary>
        /// Description:
        ///		Applies SHA256 hash on the given string
        /// Parameters:
        ///     [IN text: string - the text that will be hashed]
        /// Returns:
        ///		[string - the hashed text]
        /// </summary>
        public static string SHA256Hash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}
