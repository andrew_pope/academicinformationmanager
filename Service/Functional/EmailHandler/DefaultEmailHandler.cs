﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Service.Functional.EmailHandler
{
    /// <summary>
    /// Description:
    ///     DefaultEmailHandler uses standard library classes to send an email.
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented the DefaultEmailhandler class
    /// </summary>
    public class DefaultEmailHandler : IEmailHandler
    {
        public void SendEmail(String smtpServerAddress, String senderAddress, String senderPassword, List<String> recieverAddress, String emailSubject, String emailBody)
        {
            SmtpClient smtpServer = new SmtpClient(smtpServerAddress);
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential(senderAddress, senderPassword);
            smtpServer.EnableSsl = true;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(senderAddress);
            foreach (var address in recieverAddress)
                mail.To.Add(address);
            mail.Subject = emailSubject;
            mail.Body = emailBody;

            smtpServer.Send(mail);
        }
    }
}
