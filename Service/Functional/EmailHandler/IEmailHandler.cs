﻿using System;
using System.Collections.Generic;

namespace Service.Functional.EmailHandler
{
    /// <summary>
    /// Description:
    ///     IEmailHandler is the interface used by all Emailhandler classes
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined IEmailHandler with the following method(s): SendEmail
    /// </summary>
    interface IEmailHandler
    {
        /// <summary>
        /// Description:
        ///     Sends an email.
        /// Parameters:
        ///     [smtpServerAddress: String - the server through which the email will be sent]
        ///     [senderAddress: String - the sender's address]
        ///     [senderPassword: String - the sender's password]
        ///     [recieverAddress: String - the recievee's address]
        ///     [emailSubject: String - the email's subject]
        ///     [emailBody: String - the email's body]
        /// Returns:
        ///     [void]
        /// </summary>
        void SendEmail(String smtpServerAddress, String senderAddress, String senderPassword, List<String> recieverAddress, String emailSubject, String emailBody);
    }
}
