﻿using System.Collections.Generic;

namespace Service.Functional.Query
{
    /// <summary>
    /// Description:
    ///     Query class is used to specify a query for the repository
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined Query class with its Properties
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    public class Query
    {
        private List<string> m_tables;
        private List<string> m_displayedFields;
        private List<QueryRelationalClause> m_relationalClauses;
        private List<QueryConditionalClause> m_conditionalClauses;
        private List<QueryOrderField> m_orderFields;

        /// <summary>
        /// Description:
        ///		The class' constructor; initializes its members
        /// Parameters:
        ///     None
        /// Returns:
        ///		[void]
        /// </summary>
        public Query()
        {
            m_tables = new List<string>();
            m_displayedFields = new List<string>();
            m_relationalClauses = new List<QueryRelationalClause>();
            m_conditionalClauses = new List<QueryConditionalClause>();
            m_orderFields = new List<QueryOrderField>();
        }

        public List<string> Tables
        {
            get
            {
                return m_tables;
            }
        }

        public List<string> DisplayedFields
        {
            get
            {
                return m_displayedFields;
            }
        }

        public List<QueryRelationalClause> RelationalClauses
        {
            get
            {
                return m_relationalClauses;
            }
        }

        public List<QueryConditionalClause> ConditionalClauses
        {
            get
            {
                return m_conditionalClauses;
            }
        }

        public List<QueryOrderField> OrderFields
        {
            get
            {
                return m_orderFields;
            }
        }
    }
}
