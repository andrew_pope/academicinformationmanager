﻿using Service.Functional.Mappings;

namespace Service.Functional.Query
{
    /// <summary>
    /// Description:
    ///     QueryOrderField class is used to specify a field ordering to the Query class
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined QueryOrderField class with its Properties
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    public class QueryOrderField
    {
        private FieldMapping m_fieldMapping;
        private string m_orientation;

        /// <summary>
        /// Description:
        ///		The class' constructor
        /// Parameters:
        ///     [IN fieldMapping: FieldMapping - the field]
        ///     [IN orientation: string - the orientation ([ASC]ending or [DESC]ending)]
        /// Returns:
        ///		[void]
        /// </summary>
        public QueryOrderField(FieldMapping fieldMapping, string orientation)
        {
            m_fieldMapping = fieldMapping;
            m_orientation = orientation;
        }

        public FieldMapping FieldMapping
        {
            get
            {
                return m_fieldMapping;
            }
        }

        public string Orientation
        {
            get
            {
                return m_orientation;
            }
        }
    }
}
