﻿namespace Service.Functional.Query
{
    /// <summary>
    /// Description:
    ///     QueryRelationalClause class is used to specify a relation between tables to the Query class
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined QueryRelationalClause class with its Properties
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    public class QueryRelationalClause
    {
        private string m_lefthandFieldName;
        private string m_righthandFieldName;

        /// <summary>
        /// Description:
        ///		The class' constructor
        /// Parameters:
        ///     [IN lefthandFieldName: string - the lefthand field]
        ///     [IN righthandFieldName: string - the righhand field]
        /// Returns:
        ///		[void]
        /// </summary>
        public QueryRelationalClause(string lefthandFieldName, string righthandFieldName)
        {
            m_lefthandFieldName = lefthandFieldName;
            m_righthandFieldName = righthandFieldName;
        }

        public string LefthandFieldName
        {
            get
            {
                return m_lefthandFieldName;
            }
        }

        public string RighthandFieldName
        {
            get
            {
                return m_righthandFieldName;
            }
        }
    }
}
