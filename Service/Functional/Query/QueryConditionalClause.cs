﻿using Service.Functional.Mappings;
using System;

namespace Service.Functional.Query
{
    /// <summary>
    /// Description:
    ///     QueryConditionalClause class is used to specify a field condition to the Query class
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined QueryConditionalClause class with its Properties
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    public class QueryConditionalClause
    {
        private FieldMapping m_field;
        private Object m_value;
        private String m_operator;

        /// <summary>
        /// Description:
        ///		The class' constructor
        /// Parameters:
        ///     [IN field: FieldMapping - the field]
        ///     [IN value: Object - the value]
        ///     [IN operator: string - the relation between the field and the value]
        /// Returns:
        ///		[void]
        /// </summary>
        public QueryConditionalClause(FieldMapping field, Object value, String op)
        {
            m_field = field;
            m_value = value;
            m_operator = op;
        }

        public FieldMapping Field
        {
            get
            {
                return m_field;
            }
        }

        public Object Value
        {
            get
            {
                return m_value;
            }
        }

        public String Operator
        {
            get
            {
                return m_operator;
            }
        }
    }
}
