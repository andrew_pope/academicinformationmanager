﻿namespace Service.Functional.Mappings
{
    /// <summary>
    /// Description:
    ///     TablesRelation is a class used in RuntimeConfiguration to specify a relation between 2 tables
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined TablesRelation class with its properties
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    class TablesRelation
    {
        private string m_firstTableDBName;
        private string m_secondTableDBName;
        private string m_firstFieldName;
        private string m_secondFieldName;

        /// <summary>
        /// Description:
        ///		The class' constructor
        /// Parameters:
        ///		[IN firstTableName: string - the table's database name]
        ///     [IN firstFieldName: string - the first table's database field name used in the relation]
        ///		[IN seconfTableName: string - the table's database name]
        ///     [IN secondFieldName: string - the second table's database field name used in the relation]
        /// Returns:
        ///		[void]
        /// </summary>
        public TablesRelation(string firstTableName, string firstFieldName, string secondTableName, string secondFieldName)
        {
            m_firstTableDBName = firstTableName;
            m_firstFieldName = firstFieldName;
            m_secondTableDBName = secondTableName;
            m_secondFieldName = secondFieldName;
        }

        public string FirstTableDBName
        {
            get
            {
                return m_firstTableDBName;
            }
        }

        public string SecondTableDBName
        {
            get
            {
                return m_secondTableDBName;
            }
        }

        public string FirstFieldName
        {
            get
            {
                return m_firstFieldName;
            }
        }

        public string SecondFieldName
        {
            get
            {
                return m_secondFieldName;
            }
        }
    }
}
