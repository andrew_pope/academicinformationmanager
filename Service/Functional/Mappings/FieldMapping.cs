﻿using System;
using System.Data;

namespace Service.Functional.Mappings
{
    /// <summary>
    /// Description:
    ///     FieldMapping is a class used to map a SQL field to a C# object
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented FieldMapping class with its Properties
    /// </summary>
    public class FieldMapping
    {
        private string m_displayName;
        private Type m_displayType;
        private string m_dbName;
        private SqlDbType m_dbType;
        private Boolean m_isPrivate;

        /// <summary>
        /// Description:
        ///		The class' constructor
        /// Parameters:
        ///     [IN displayName: string - the name that will be displayed to the user]
        ///     [IN displayType: Type - the C# counterpart of the SQL type]
        ///     [IN dbName: string - the field's name in the database]
        ///     [IN dbType: SqlDbType - the field's type in SQL]
        ///     [IN isPrivate: boolean - specifies if the field will pass to the Controller]
        /// Returns:
        ///		[string - the hashed text]
        /// </summary>
        public FieldMapping(string displayName, Type displayType, string dbName, SqlDbType dbType, Boolean isPrivate)
        {
            m_displayName = displayName;
            m_displayType = displayType;
            m_dbName = dbName;
            m_dbType = dbType;
            m_isPrivate = isPrivate;
        }

        public string DisplayName
        {
            get
            {
                return m_displayName;
            }
        }

        public Type DisplayType
        {
            get
            {
                return m_displayType;
            }
        }

        public string DBName
        {
            get
            {
                return m_dbName;
            }
        }

        public SqlDbType DBType
        {
            get
            {
                return m_dbType;
            }
        }

        public Boolean IsPrivate
        {
            get
            {
                return m_isPrivate;
            }
        }
    }
}
