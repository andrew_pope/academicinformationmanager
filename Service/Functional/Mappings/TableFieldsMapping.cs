﻿using System.Collections.Generic;

namespace Service.Functional.Mappings
{
    /// <summary>
    /// Description:
    ///     TableFieldsMapping is a class used in RuntimeConfiguration to specify an entire database record with its fields
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined TableFieldsMapping with its Properties
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    class TableFieldsMapping
    {
        private string m_dbName;
        private string m_displayName;
        private List<FieldMapping> m_fieldMappings;

        /// <summary>
        /// Description:
        ///		The class' constructor; initializes a table with no fields (the fields will be added after)
        /// Parmeters:
        ///		[IN dbName: string - the table's database name]
        ///     [IN displayName: string - the table's display name]
        /// Returns:
        ///		[void]
        /// </summary>
        public TableFieldsMapping(string dbName, string displayName)
        {
            m_dbName = dbName;
            m_displayName = displayName;
            m_fieldMappings = new List<FieldMapping>();
        }

        public string DBName
        {
            get
            {
                return m_dbName;
            }
        }

        public string DisplayName
        {
            get
            {
                return m_displayName;
            }
        }

        public List<FieldMapping> FieldMappings
        {
            get
            {
                return m_fieldMappings;
            }
        }
    }
}
