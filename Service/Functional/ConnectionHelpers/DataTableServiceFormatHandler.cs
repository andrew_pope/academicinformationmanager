﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Service.Functional.ConnectionHelpers
{
    /// <summary>
    /// Description:
    ///     DataTableServiceFormatHandler is used to convert infromation from the DataTable type
    ///     to the DataTableServiceFormat class and vice-versa
    /// Created by Andrei Popa on 07/04/2016
    ///     -Defined and implemented DataTableServiceFormatHandler with the following method(s): ToDataTable, FromDataTable
    /// </summary>
    public static class DataTableServiceFormatHandler
    {
        /// <summary>
        /// Description:
        ///     Converts data from the DataTable format into the DataTableServiceFormat class
        /// Parameters:
        ///     [data: DataTable - the data to be converted]
        /// Returns:
        ///     [DataTableServiceFormat - the converted data]
        /// </summary>
        public static DataTableServiceFormat FromDataTable(DataTable data)
        {
            DataTableServiceFormat convertedData = new DataTableServiceFormat();
            List<string> headers = new List<string>();
            List<object> values = new List<object>();
            foreach (DataColumn column in data.Columns)
            {
                headers.Add(column.ColumnName);
            }
            foreach (DataRow row in data.Rows)
            {
                foreach (DataColumn column in data.Columns)
                {
                    values.Add(row[column]);
                }
            }
            convertedData.Headers = headers.ToArray();
            convertedData.Values = values.ToArray();
            return convertedData;
        }

        /// <summary>
        /// Description:
        ///     Converts data from DataTableServiceFormat into DataTable
        /// Parameters:
        ///     [data: DataTableServiceFormat - the data to be converted]
        /// Returns:
        ///     [DataTable - the converted data]
        /// </summary>
        public static DataTable ToDataTable(DataTableServiceFormat data)
        {
            DataTable convertedData = new DataTable();
            List<string> headers = new List<string>(data.Headers);
            List<object> values = new List<object>(data.Values);
            foreach (string columnName in headers)
            {
                convertedData.Columns.Add(columnName);
            }
            int columnCount = data.Headers.Count();
            if (columnCount > 0)
            {
                int rowCount = data.Values.Count() / columnCount;
                for (int i = 0; i < rowCount; ++i)
                {
                    List<object> rowValues = new List<object>();
                    for (int j = 0; j < columnCount; ++j)
                    {
                        rowValues.Add(values[i * columnCount + j]);
                    }
                    convertedData.Rows.Add(rowValues.ToArray());
                }
            }
            return convertedData;
        }
    }
}