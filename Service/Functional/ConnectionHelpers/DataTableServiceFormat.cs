﻿using System.Runtime.Serialization;

namespace Service.Functional.ConnectionHelpers
{
    /// <summary>
    /// Description:
    ///     DataTableServiceFormat is used for communications between the host and its clients
    ///     ensuring that the data is serializable, using primitive data types
    /// Created by Andrei Popa on 07/04/2016
    ///     -Defined the DataServiceFormat class
    /// </summary>
    [DataContract]
    public class DataTableServiceFormat
    {
        [DataMember]
        public string[] Headers { get; set; }

        [DataMember]
        public object[] Values { get; set; }
    }
}
