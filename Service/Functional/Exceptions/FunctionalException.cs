﻿using System;

namespace Service.Functional.Exceptions
{
    /// <summary>
    /// Description:
    ///     FunctionalException is the custom exception class that will be thrown inside the Functional layer
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented FunctionalException class
    /// </summary>
    class FunctionalException : BaseException
    {
        /// <summary>
        /// Description:
        ///     The FunctionalException class' bare constructor.
        /// Parameters:
        ///     [None]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public FunctionalException() : base() { }

        /// <summary>
        /// Description:
        ///     The FunctionalException class' simple constructor.
        /// Parameters:
        ///     [message: String - details the exception's cause]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public FunctionalException(String message) : base(message) { }

        /// <summary>
        /// Description:
        ///     The FunctionalException class' complex constructor.
        /// Parameters:
        ///     [causeException: Exception - the exception that caused this exceptions (if this exception is used to propagate the exception further)]
        ///     [userMessage: String - the message that wil be displayed to the user]
        ///     [developerMessage: String - the message that will be logged for the developer]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public FunctionalException(Exception causeException, String userMessage, String developerMessage) : base(causeException, userMessage, developerMessage) { }
    }
}
