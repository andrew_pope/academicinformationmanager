﻿using System;

namespace Service.Functional.Exceptions
{
    /// <summary>
    /// Description:
    ///     DataAccessException is the custom exception class that will be thrown inside the DataAccess layer
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented RepositoryException class
    /// </summary>
    public class DataAccessException : BaseException
    {
        /// <summary>
        /// Description:
        ///     The DataAccessException class' bare constructor.
        /// Parameters:
        ///     [None]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public DataAccessException() : base() { }

        /// <summary>
        /// Description:
        ///     The DataAccessException class' simple constructor.
        /// Parameters:
        ///     [message: String - details the exception's cause]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public DataAccessException(String message) : base(message) { }

        /// <summary>
        /// Description:
        ///     The DataAccessException class' complex constructor.
        /// Parameters:
        ///     [causeException: Exception - the exception that caused this exceptions (if this exception is used to propagate the exception further)]
        ///     [userMessage: String - the message that wil be displayed to the user]
        ///     [developerMessage: String - the message that will be logged for the developer]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public DataAccessException(Exception causeException, String userMessage, String developerMessage) : base(causeException, userMessage, developerMessage) { }
    }
}
