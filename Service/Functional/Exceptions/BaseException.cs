﻿using System;

namespace Service.Functional.Exceptions
{
    /// <summary>
    /// Description:
    ///     BaseException is the base class inherited by all custome exceptions used in this project
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented BaseException class
    /// </summary>
    public class BaseException : Exception
    {
        private Exception m_causeException;
        private String m_userMessage;
        private String m_developerMessage;

        /// <summary>
        /// Description:
        ///     The BaseException class' bare constructor.
        /// Parameters:
        ///     [None]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public BaseException() : base() { }

        /// <summary>
        /// Description:
        ///     The BaseException class' simple constructor.
        /// Parameters:
        ///     [message: String - details the exception's cause]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public BaseException(String message) : base(message) { m_causeException = null; m_userMessage = message; m_developerMessage = message; }

        /// <summary>
        /// Description:
        ///     The BaseException class' complex constructor.
        /// Parameters:
        ///     [causeException: Exception - the exception that caused this exceptions (if this exception is used to propagate the exception further)]
        ///     [userMessage: String - the message that wil be displayed to the user]
        ///     [developerMessage: String - the message that will be logged for the developer]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public BaseException(Exception causeException, String userMessage, String developerMessage) : base(developerMessage)
        {
            m_causeException = causeException;
            m_userMessage = userMessage;
            m_developerMessage = developerMessage;
        }

        /// <summary>
        /// Properties
        ///     CauseException      - read-only
        ///     UserMessage         - read-only
        ///     DeveloperMessage    - read-only
        /// </summary>
        public Exception CauseException
        {
            get
            {
                return m_causeException;
            }
        }
        public String UserMessage
        {
            get
            {
                return m_userMessage;
            }
        }
        public String DeveloperMessage
        {
            get
            {
                return m_developerMessage;
            }
        }
    }
}
