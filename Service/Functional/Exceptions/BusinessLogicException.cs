﻿using System;

namespace Service.Functional.Exceptions
{
    /// <summary>
    /// Description:
    ///     BusinessLogicException is the custom exception class that will be thrown inside the BusinessLogic layer
    /// Created by Andrei Popa on 22/03/2016
    ///     -Defined and implemented ControllerException class
    /// Modified by Andrei Popa on 07/04/2016
    ///     -Renamed class to BusinessLogicException
    /// </summary>
    class BusinessLogicException : BaseException
    {
        /// <summary>
        /// Description:
        ///     The BusinessLogicException class' bare constructor.
        /// Parameters:
        ///     [None]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public BusinessLogicException() : base() { }

        /// <summary>
        /// Description:
        ///     The BusinessLogicException class' simple constructor.
        /// Parameters:
        ///     [message: String - details the exception's cause]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public BusinessLogicException(String message) : base(message) { }

        /// <summary>
        /// Description:
        ///     The BusinessLogicException class' complex constructor.
        /// Parameters:
        ///     [causeException: Exception - the exception that caused this exceptions (if this exception is used to propagate the exception further)]
        ///     [userMessage: String - the message that wil be displayed to the user]
        ///     [developerMessage: String - the message that will be logged for the developer]
        /// Returns:
        ///     [Nothing]
        /// </summary>
        public BusinessLogicException(Exception causeException, String userMessage, String developerMessage) : base(causeException, userMessage, developerMessage) { }
    }
}
