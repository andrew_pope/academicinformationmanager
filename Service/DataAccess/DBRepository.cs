﻿using Service.Functional;
using Service.Functional.Exceptions;
using Service.Functional.Mappings;
using Service.Functional.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Service.DataAccess
{
    /// <summary>
    /// Description:
    ///     DBRepository is a repository class that communicates with a database
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined IRepo interface with the following method(s): Create, Read, Update, Delete, ProcessOutputData
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Extended the Read method and created an aditional one
    /// </summary>
    public class DBRepository : IRepository
    {
        SqlConnection m_connection;
        List<String> m_databaseOutput;

        public DBRepository()
        {
            m_connection = new SqlConnection(RuntimeConfiguration.DBConnectionString);
            m_databaseOutput = new List<String>();
            m_connection.InfoMessage += delegate (object sender, SqlInfoMessageEventArgs e) { m_databaseOutput.Add(e.Message); };
        }

        public void Create(DataRow newData)
        {
            TableFieldsMapping mapping = RuntimeConfiguration.TableFieldMappings[(string)newData["TableName"]];
            String sqlCommandString = "insert into " + mapping.DBName + " values('[placeholder]',";
            for (int index = 1; index < mapping.FieldMappings.Count - 1; ++index)
                sqlCommandString += "@" + index + ",";
            sqlCommandString += "@" + (mapping.FieldMappings.Count - 1) + ")";
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, m_connection);
            for (int index = 1; index < mapping.FieldMappings.Count; ++index)
            {
                string parameterName = "@" + index;
                sqlCommand.Parameters.Add(parameterName, mapping.FieldMappings[index].DBType);
                sqlCommand.Parameters[parameterName].Value = 
                    Convert.ChangeType(
                        newData[mapping.FieldMappings[index].DisplayName], 
                        mapping.FieldMappings[index].DisplayType);
            }
            try
            {
                m_connection.Open();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "Could not connect to database! Try again!", 
                    "The user failed to connect to a SQL databse with the following connection string: " + m_connection.ConnectionString);
            }
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "An unexpected error has occurred, please contact the administrator!", 
                    "The following exeption was caught during an update to the database: " + e.Message);
            }
            finally
            {
                m_connection.Close();
            }
            DataAccessException exception = null;
            for (int i = 0; i < m_databaseOutput.Count; ++i)
            {
                string[] messages = m_databaseOutput[i].Split('|');
                exception = new DataAccessException(exception, messages[0], messages[1]);
            }
            if (exception != null)
                throw exception;
        }

        public void Delete(DataRow deletedData)
        {
            TableFieldsMapping mapping = RuntimeConfiguration.TableFieldMappings[(string)deletedData["TableName"]];
            String sqlCommandString = "delete from " + mapping.DBName + " where " + mapping.FieldMappings[0].DBName + " = @0";
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, m_connection);
            sqlCommand.Parameters.Add("@0", mapping.FieldMappings[0].DBType);
            sqlCommand.Parameters["@0"].Value = 
                Convert.ChangeType(
                    deletedData[mapping.FieldMappings[0].DisplayName], 
                    mapping.FieldMappings[0].DisplayType);
            try
            {
                m_connection.Open();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "Could not connect to database! Try again!", 
                    "The user failed to connect to a SQL databse with the following connection string: " + m_connection.ConnectionString);
            }
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "An unexpected error has occurred, please contact the administrator!", 
                    "The following exeption was caught during an update to the database: " + e.Message);
            }
            finally
            {
                m_connection.Close();
            }
            DataAccessException exception = null;
            for (int i = 0; i < m_databaseOutput.Count; ++i)
            {
                string[] messages = m_databaseOutput[i].Split('|');
                exception = new DataAccessException(exception, messages[0], messages[1]);
            }
            if (exception != null)
                throw exception;
        }

        public DataTable Read(Query query)
        {
            DataTable data = new DataTable();
            string sqlCommandString = "select ";
            for (int i = 0; i < query.DisplayedFields.Count - 1; ++i)
                sqlCommandString += query.DisplayedFields[i] + ", ";
            sqlCommandString += query.DisplayedFields.Last() + " ";
            sqlCommandString += "from ";
            for (int i = 0; i < query.Tables.Count - 1; ++i)
                sqlCommandString += query.Tables[i] + ", ";
            sqlCommandString += query.Tables.Last() + " ";
            if (query.RelationalClauses.Count > 0 || query.ConditionalClauses.Count > 0)
            {
                sqlCommandString += "where ";
                for (int i = 0; i < query.RelationalClauses.Count - 1; ++i)
                {
                    sqlCommandString += 
                        query.RelationalClauses[i].LefthandFieldName + " = " + 
                        query.RelationalClauses[i].RighthandFieldName + " and ";
                }
                if (query.RelationalClauses.Count > 0)
                    sqlCommandString += 
                        query.RelationalClauses.Last().LefthandFieldName + " = " + 
                        query.RelationalClauses.Last().RighthandFieldName + " ";
                if (query.ConditionalClauses.Count > 0 && query.RelationalClauses.Count > 0)
                    sqlCommandString += "and ";
                for (int i = 0; i < query.ConditionalClauses.Count - 1; ++i)
                {
                    sqlCommandString += 
                        query.ConditionalClauses[i].Field.DBName + " " + 
                        query.ConditionalClauses[i].Operator + " @" + i + " and ";
                }
                if (query.RelationalClauses.Count > 0)
                    sqlCommandString += 
                        query.ConditionalClauses.Last().Field.DBName + " " + 
                        query.ConditionalClauses.Last().Operator + " @" + (query.ConditionalClauses.Count - 1) + " ";
                if (query.OrderFields.Count > 0)
                {
                    sqlCommandString += "order by ";
                    for (int i = 0; i < query.OrderFields.Count - 1; ++i)
                        sqlCommandString += 
                            query.OrderFields[i].FieldMapping.DBName + " " + 
                            query.OrderFields[i].Orientation + ", ";
                    sqlCommandString += 
                        query.OrderFields.Last().FieldMapping.DBName + " " + 
                        query.OrderFields.Last().Orientation + " ";
                }
            }
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, m_connection);
            for (int i = 0; i < query.ConditionalClauses.Count; ++i)
            {
                sqlCommand.Parameters.Add("@" + i, query.ConditionalClauses[i].Field.DBType);
                sqlCommand.Parameters["@" + i].Value = 
                    Convert.ChangeType(
                        query.ConditionalClauses[i].Value, 
                        query.ConditionalClauses[i].Field.DisplayType);
            }
            try
            {
                m_connection.Open();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "Could not connect to database! Try again!", 
                    "The user failed to connect to the SQL database with the following connection string: " + m_connection.ConnectionString);
            }
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                data.Load(sqlDataReader);
                m_connection.Close();
            }
            DataAccessException exception = null;
            for (int i = 0; i < m_databaseOutput.Count; ++i)
            {
                string[] messages = m_databaseOutput[i].Split('|');
                exception = new DataAccessException(exception, messages[0], messages[1]);
            }
            if (exception != null)
                throw exception;
            this.ProcessOutputData(data);
            return data;
        }

        public DataTable Read(string sqlCommandString)
        {
            DataTable data = new DataTable();
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, m_connection);
            try
            {
                m_connection.Open();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "Could not connect to database! Try again!", 
                    "The user failed to connect to the SQL database with the following connection string: " + m_connection.ConnectionString);
            }
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                data.Load(sqlDataReader);
                m_connection.Close();
            }
            DataAccessException exception = null;
            for (int i = 0; i < m_databaseOutput.Count; ++i)
            {
                string[] messages = m_databaseOutput[i].Split('|');
                exception = new DataAccessException(exception, messages[0], messages[1]);
            }
            if (exception != null)
                throw exception;
            this.ProcessOutputData(data);
            return data;

        }

        public void Update(DataRow updatedData)
        {
            TableFieldsMapping mapping = RuntimeConfiguration.TableFieldMappings[(string)updatedData["TableName"]];
            String sqlCommandString = "update " + mapping.DBName + " set ";
            for (int index = 1; index < mapping.FieldMappings.Count - 1; ++index)
                sqlCommandString += mapping.FieldMappings[index].DBName + " = @" + index + ",";
            sqlCommandString += mapping.FieldMappings.Last().DBName + " = @" + (mapping.FieldMappings.Count - 1) + " where ";
            sqlCommandString += mapping.FieldMappings.First().DBName + " = @0";
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, m_connection);
            for (int index = 0; index < mapping.FieldMappings.Count; ++index)
            {
                string parameterName = "@" + index;
                sqlCommand.Parameters.Add(parameterName, mapping.FieldMappings[index].DBType);
                sqlCommand.Parameters[parameterName].Value = 
                    Convert.ChangeType(
                        updatedData[mapping.FieldMappings[index].DisplayName], 
                        mapping.FieldMappings[index].DisplayType);
            }
            try
            {
                m_connection.Open();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "Could not connect to database! Try again!", 
                    "The user failed to connect to a SQL databse with the following connection string: " + m_connection.ConnectionString);
            }
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new DataAccessException(
                    e, 
                    "An unexpected error has occurred, please contact the administrator!", 
                    "The following exeption was caught during an update to the database: " + e.Message);
            }
            finally
            {
                m_connection.Close();
            }
            DataAccessException exception = null;
            for (int i = 0; i < m_databaseOutput.Count; ++i)
            {
                string[] messages = m_databaseOutput[i].Split('|');
                exception = new DataAccessException(exception, messages[0], messages[1]);
            }
            if (exception != null)
                throw exception;
        }

        /// <summary>
        /// Description:
        ///		Processes output data before it is passed to the Controller 
        ///     (hides private data and changes headers to display names)
        /// Parmeters:
        ///		[IN/OUT data: DataTable - the processed data]
        /// Returns:
        ///		[void]
        /// </summary>
        private void ProcessOutputData(DataTable data)
        {
            List<string> delete = new List<string>();
            foreach (DataColumn column in data.Columns)
            {
                bool found = false;
                foreach (TableFieldsMapping classMapping in RuntimeConfiguration.TableFieldMappings.Values)
                {
                    FieldMapping fieldMapping = classMapping.FieldMappings.Find(x => x.DBName.Equals(column.ColumnName));
                    if (fieldMapping != null)
                    {
                        found = true;
                        if (fieldMapping.IsPrivate)
                            delete.Add(column.ColumnName);
                        else
                            column.ColumnName = fieldMapping.DisplayName;
                        break;
                    }
                }
                if (!found)
                    throw new DataAccessException(
                        null, 
                        "An internal error has occurred! Please contact the administrator!", 
                        "There was no display name found to the following database column: " + column.ColumnName);
            }
            foreach (string columnName in delete)
            {
                data.Columns.Remove(columnName);
            }
        }
    }
}
