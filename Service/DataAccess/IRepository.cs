﻿using Service.Functional.Query;
using System.Data;

namespace Service.DataAccess
{
    /// <summary>
    /// Description:
    ///     IRepository is the interface used by all Repository classes
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined IRepo interface with the following method(s): Create, Read (2 overloads), Update, Delete
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Description:
        ///		Adds a new entity in the repository.
        /// Parmeters:
        ///		[IN newData: DataRow - the data that will be added to the repository]
        /// Returns:
        ///		[void]
        /// </summary>
        void Create(DataRow newData);

        /// <summary>
        /// Description:
        ///		Reads all entities from repository, filtered and sorted.
        /// Parmeters:
        ///     [IN query: Query - a query detailing the searched data]
        /// Returns:
        ///		[DataTable - a table containing the searched data]
        /// </summary>
        DataTable Read(Query query);

        /// <summary>
        /// Warning:
        ///     This should not be used with user input as there is no SQL injection prevention implemented.
        /// Description:
        ///		Reads all entities from repository, using a custom query specified by the caller.
        /// Parmeters:
        ///     [IN query: sqlCommandString - a query detailing the searched data]
        /// Returns:
        ///		[DataTable - a table containing the searched data]
        /// </summary>
        DataTable Read(string sqlCommandString);

        /// <summary>
        /// Description:
        ///		Updates the values of an entity in the repository.
        /// Parmeters:
        ///		[IN updatedData: DataRow - the data that will be updated]
        /// Returns:
        ///		[void]
        /// </summary>
        void Update(DataRow updatedData);

        /// <summary>
        /// Description:
        ///		Removes an entity from the repository.
        /// Parmeters:
        ///		[IN deletedData: DataRow - the data that will be deleted]
        /// Returns:
        ///		[void]
        /// </summary>
        void Delete(DataRow deletedData);
    }
}
