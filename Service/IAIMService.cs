﻿using Service.Functional.ConnectionHelpers;
using System.ServiceModel;

namespace Service
{
    /// <summary>
    /// Description:
    ///     IAIMService is the interface used by the web service, that can be accessed by the clients
    /// Created by Andrei Popa on 07/04/2016
    ///     -Defined the IAIMService interface with te following method(s): Add, Modify, Remove, Filter
    /// </summary>
    [ServiceContract]
    public interface IAIMService
    {
        /// <summary>
        /// Description:
        ///		Stores a new entity.
        /// Parmeters:
        ///		[IN data: DataTable - a table containing the data to be added]
        /// Returns:
        ///		[void]
        /// </summary>
        [OperationContract, FaultContract(typeof(string))]
        void Add(DataTableServiceFormat data);

        /// <summary>
        /// Description:
        ///		Modifies an entity.
        /// Parmeters:
        ///		[IN data: DataTable - a table containing the data to be modified]
        /// Returns:
        ///		[void]
        /// </summary>
        [OperationContract, FaultContract(typeof(string))]
        void Modify(DataTableServiceFormat data);

        /// <summary>
        /// Description:
        ///		Removes an entity.
        /// Parmeters:
        ///		[IN data: DataTable - a table containing the data to be removed]
        /// Returns:
        ///		[void]
        /// </summary>
        [OperationContract, FaultContract(typeof(string))]
        void Remove(DataTableServiceFormat data);

        /// <summary>
        /// Description:
        ///		Returns filtered data from the database.
        /// Parmeters:
        ///		[IN tableNames: DataTable - the tables that will be used searchng for the data]
        ///     [IN displayFields: DataTable - the fields that will be brought from the repository]
        ///     [IN filters: DataTable - the filtered values]
        ///     [IN order: DataTable - the order in which the data will arranged]
        /// Returns:
        ///		[DataTable - the filtered data]
        /// </summary>
        [OperationContract, FaultContract(typeof(string))]
        DataTableServiceFormat Filter(
            DataTableServiceFormat tableNames, 
            DataTableServiceFormat displayFields, 
            DataTableServiceFormat filters, 
            DataTableServiceFormat order);
    }
}
