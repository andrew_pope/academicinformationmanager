﻿using Service.Functional.Mappings;
using System;
using System.Collections.Generic;
using System.Data;

namespace Service.Functional
{
    /// <summary>
    /// Description:
    ///     RuntimeConfiguration class configures the entire backend to be able to properly communicate with the database
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined RuntimeConfiguration class with the following methods: LoadConfiguration
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the class
    /// </summary>
    static class RuntimeConfiguration
    {
        private static string _dbConnectionString;
        private static string _errorLog;
        private static string _informationLog;
        private static IDictionary<string, TableFieldsMapping> _tableFieldMappings;
        private static IList<TablesRelation> _relations;

        /// <summary>
        /// Description:
        ///		The class' constructor; initializes its member and calls the LoadConfiguration method
        /// Parameters:
        ///     None
        /// Returns:
        ///		[void]
        /// </summary>
        static RuntimeConfiguration()
        {
            _tableFieldMappings = new Dictionary<string, TableFieldsMapping>();
            _relations = new List<TablesRelation>();
            LoadConfiguration();
        }

        /// <summary>
        /// Description:
        ///		Loads the class with hardcoded configurations
        /// Parmeters:
        ///		[IN location: string - the locaton of the configuration file] (future development)
        /// Returns:
        ///		[void]
        /// </summary>
        public static void LoadConfiguration(string location = "config.txt")
        {
            _dbConnectionString = "Data Source=MSI;Initial Catalog=AIMDatabase;Integrated Security=True";
            _errorLog = "aimservices_error.log";
            _informationLog = "aimservices_information.log";
            TableFieldsMapping tempClassMappings = new TableFieldsMapping("Account", "Account");
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountID", typeof(String), "accID", SqlDbType.NVarChar, false));
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountUsername", typeof(String), "accUsername", SqlDbType.NVarChar, false));
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountValidationCode", typeof(String), "accValidationCode", SqlDbType.NVarChar, true));
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountType", typeof(String), "accType", SqlDbType.NVarChar, false));
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountEmail", typeof(String), "accEmail", SqlDbType.NVarChar, false));
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountName", typeof(String), "accName", SqlDbType.NVarChar, false));
            tempClassMappings.FieldMappings.Add(new FieldMapping("AccountApproved", typeof(Boolean), "accApproved", SqlDbType.Bit, false));
            _tableFieldMappings.Add(tempClassMappings.DisplayName, tempClassMappings);
            _relations.Add(new TablesRelation("Account", "accID", "Student", "stdAccountID"));
            _relations.Add(new TablesRelation("Account", "accID", "Teacher", "tchAccountID"));
        }

        public static string DBConnectionString
        {
            get
            {
                return _dbConnectionString;
            }
        }

        public static string ErrorLog
        {
            get
            {
                return _errorLog;
            }
        }

        public static string InformationLog
        {
            get
            {
                return _informationLog;
            }
        }

        public static IDictionary<string, TableFieldsMapping> TableFieldMappings
        {
            get
            {
                return _tableFieldMappings;
            }
        }

        public static IList<TablesRelation> Relations
        {
            get
            {
                return _relations;
            }
        }
    }
}
