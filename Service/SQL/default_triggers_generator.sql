﻿USE [AIMDatabase]

/* Add triggers */
GO
CREATE TRIGGER trigAccC ON Account
INSTEAD OF INSERT
AS
	DECLARE @username NVARCHAR(256)
	DECLARE @validationCode NVARCHAR(256)
	DECLARE @type NVARCHAR(256)
	DECLARE @email NVARCHAR(256)
	DECLARE @name NVARCHAR(256)
	DECLARE @approved NVARCHAR(256)
	
	SELECT @username = accUsername, @validationCode = accValidationCode, @type = accType, @email = accEmail, @name = accName, @approved = accApproved
	FROM inserted
	IF (SELECT COUNT(*) FROM Account WHERE accUsername = @username) > 0
	BEGIN
		PRINT('The username is already used!')
		RETURN
	END
	IF @type != 'Student' AND @type != 'Teacher' AND @type != 'Admin'
	BEGIN
		PRINT('The account type is not valid! Must be "Student", "Teacher" or "Admin"!')
		RETURN
	END
	IF (SELECT COUNT(*) FROM Account WHERE accEmail = @email) > 0
	BEGIN
		PRINT('The email is already linked to an account!')
		RETURN
	END
	INSERT INTO Account
	VALUES(CONVERT(NVARCHAR(256), NEWID()), @username, @validationCode, @type, @email, @name, @approved)
GO

GO
CREATE TRIGGER trigAccU ON Account
INSTEAD OF UPDATE
AS
	DECLARE @ID NVARCHAR(256)
	DECLARE @old_username NVARCHAR(256)
	DECLARE @old_type NVARCHAR(256)
	DECLARE @old_email NVARCHAR(256)
	DECLARE @new_username NVARCHAR(256)
	DECLARE @new_validation_code NVARCHAR(256)
	DECLARE @new_type NVARCHAR(256)
	DECLARE @new_email NVARCHAR(256)
	DECLARE @new_name NVARCHAR(256)
	DECLARE @new_apporved BIT

	SELECT @ID = accID, 
		   @new_username = accUsername,
		   @new_validation_code = accValidationCode, 
		   @new_type = accType, 
		   @new_email = accEmail,
		   @new_name = accName,
		   @new_apporved = accApproved
	FROM inserted
	IF (SELECT COUNT(*) FROM Account WHERE accID = @ID) = 0
	BEGIN
		PRINT('There is no record with the given ID!')
		RETURN
	END
	SELECT @old_username = accUsername, @old_type = accType, @old_email = accEmail
	FROM Account
	WHERE accID = @ID
	IF @new_username != @old_username
	BEGIN
		PRINT('Cannot change username!')
		RETURN
	END
	IF @new_type != @old_type
	BEGIN
		PRINT('Cannot change account type!')
		RETURN
	END
	IF @new_email != @old_email
	BEGIN
		PRINT('Cannot change email!')
		RETURN
	END
	UPDATE Account
	SET accUsername = @new_username, accValidationCode = @new_validation_code, accType = @new_type, accEmail = @new_email, accName = @new_name, accApproved = @new_apporved
	WHERE accID = @ID
GO

GO
CREATE TRIGGER trigAccD ON Account
INSTEAD OF DELETE
AS
	DECLARE @ID NVARCHAR(256)

	SELECT @ID = accID
	FROM deleted
	IF (SELECT COUNT(*) FROM Account WHERE accID = @ID) = 0
	BEGIN
		PRINT('There is no record with the given ID!')
		RETURN
	END
	IF (SELECT COUNT(*) FROM Student WHERE stdAccountID = @ID) > 0
	BEGIN
		PRINT('There are records referencing this account!')
		RETURN
	END
	IF (SELECT COUNT(*) FROM Teacher WHERE tchAccountID = @ID) > 0
	BEGIN
		PRINT('There are records referencing this account!')
		RETURN
	END
	DELETE Account
	WHERE accID = @ID
GO