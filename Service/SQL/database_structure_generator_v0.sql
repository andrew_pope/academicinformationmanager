﻿USE [AIMDatabase]

/* Create tables */
CREATE TABLE AcademicYearState
(
	aysID NVARCHAR(256) NOT NULL,
	aysCurrentState NVARCHAR(256) NOT NULL
	CONSTRAINT pk_AcademicYearStateID PRIMARY KEY (aysID)
);

CREATE TABLE Account
(
	accID NVARCHAR(256) NOT NULL,
	accUsername NVARCHAR(256) NOT NULL,
	accValidationCode NVARCHAR(256) NOT NULL,
	accType NVARCHAR(256) NOT NULL,
	accEmail NVARCHAR(256) NOT NULL,
	accName NVARCHAR(256) NOT NULL,
	accApproved NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_AccountID PRIMARY KEY (accID)
);

CREATE TABLE Activity
(
	actID NVARCHAR(256) NOT NULL,
	actTeacherID NVARCHAR(256) NOT NULL,
	actDisciplineSemesterID NVARCHAR(256) NOT NULL,
	actType NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_ActivityID PRIMARY KEY (actID)
);

CREATE TABLE Department
(
	depID NVARCHAR(256) NOT NULL,
	depHeadID NVARCHAR(256) NOT NULL,
	depFacultyID NVARCHAR(256) NOT NULL,
	depName NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_DepartmentID PRIMARY KEY (depID)
);

CREATE TABLE Discipline
(
	dscID NVARCHAR(256) NOT NULL,
	dscName NVARCHAR(256) NOT NULL,
	dscType NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_DisciplineID PRIMARY KEY (dscID)
);

CREATE TABLE DisciplineSemester
(
	disID NVARCHAR(256) NOT NULL,
	disDisciplineID NVARCHAR(256) NOT NULL,
	disSemesterID NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_DisciplineSemesterID PRIMARY KEY (disID)
);

CREATE TABLE Evaluation
(
	evlID NVARCHAR(256) NOT NULL,
	evlStudentID NVARCHAR(256) NOT NULL,
	evlDisciplineSemesterID NVARCHAR(256) NOT NULL,
	evlGrade INT NOT NULL,
	CONSTRAINT pk_EvaluationID PRIMARY KEY (evlID)
);

CREATE TABLE Faculty
(
	facID NVARCHAR(256) NOT NULL,
	facName NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_FacultyID PRIMARY KEY (facID)
);

CREATE TABLE [Group]
(
	grpID NVARCHAR(256) NOT NULL,
	grpSemesterID NVARCHAR(256) NOT NULL,
	grpName NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_GroupID PRIMARY KEY (grpID)
);

CREATE TABLE ProposedDiscipline
(
	prdID NVARCHAR(256) NOT NULL ,
	prdTeacherID NVARCHAR(256) NOT NULL,
	prdDisciplineSemesterID NVARCHAR(256) NOT NULL,
	prdPriority INT NOT NULL,
	CONSTRAINT pk_ProposedDisciplineID PRIMARY KEY (prdID)
);

CREATE TABLE SelectedDiscipline
(
	sldID NVARCHAR(256) NOT NULL ,
	sldStudentID NVARCHAR(256) NOT NULL,
	sldDisciplineSemesterID NVARCHAR(256) NOT NULL,
	sldPriority INT NOT NULL,
	CONSTRAINT pk_SelectedDisciplineID PRIMARY KEY (sldID)
);

CREATE TABLE Semester
(
	semID NVARCHAR(256) NOT NULL ,
	semFacultyID NVARCHAR(256) NOT NULL,
	semStudyLevel NVARCHAR(256) NOT NULL,
	semSpecialty NVARCHAR(256) NOT NULL,
	semNumber INT NOT NULL,
	semYear INT NOT NULL,
	CONSTRAINT pk_SemesterID PRIMARY KEY (semID)
);

CREATE TABLE Student
(
	stdID NVARCHAR(256) NOT NULL ,
	stdAccountID NVARCHAR(256) NOT NULL,
	stdStatus NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_StudentID PRIMARY KEY (stdID)
);

CREATE TABLE StudentGroup
(
	stgID NVARCHAR(256) NOT NULL ,
	stgStudentID NVARCHAR(256) NOT NULL,
	stgGroupID NVARCHAR(256) NOT NULL,
	stgSignedContract BIT NOT NULL,
	CONSTRAINT pk_StudentGroupID PRIMARY KEY (stgID)
);

CREATE TABLE Teacher
(
	tchID NVARCHAR(256) NOT NULL ,
	tchAccountID NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_TeacherID PRIMARY KEY (tchID)
);

CREATE TABLE TeacherDepartment
(
	tdeID NVARCHAR(256) NOT NULL ,
	tdeTeacherID NVARCHAR(256) NOT NULL,
	tdeDepartmentID NVARCHAR(256) NOT NULL
	CONSTRAINT pk_TeacherDepartmentID PRIMARY KEY (tdeID)
);

CREATE TABLE AcceptedDiscipline
(
	acdID NVARCHAR(256) NOT NULL,
	acdDepartmentID NVARCHAR(256) NOT NULL,
	acdDisciplineSemesterID NVARCHAR(256) NOT NULL,
	CONSTRAINT pk_AcceptedDisciplinesID PRIMARY KEY (acdID)
);


/* Add foreign key constraints */
ALTER TABLE Activity
ADD CONSTRAINT fk_ActTchID
FOREIGN KEY (actTeacherID)
REFERENCES Teacher(tchID)
ALTER TABLE Activity
ADD CONSTRAINT fk_ActDisID
FOREIGN KEY (actDisciplineSemesterID)
REFERENCES DisciplineSemester(disID)

ALTER TABLE Department
ADD CONSTRAINT fk_DepTchID
FOREIGN KEY (depHeadID)
REFERENCES Teacher(tchID)
ALTER TABLE Department
ADD CONSTRAINT fk_DepFacID
FOREIGN KEY (depFacultyID)
REFERENCES Faculty(facID)

ALTER TABLE DisciplineSemester
ADD CONSTRAINT fk_DisDscID
FOREIGN KEY (disDisciplineID)
REFERENCES Discipline(dscID)
ALTER TABLE DisciplineSemester
ADD CONSTRAINT fk_DisSemID
FOREIGN KEY (disSemesterID)
REFERENCES Semester(semID)

ALTER TABLE Evaluation
ADD CONSTRAINT fk_EvlStdID
FOREIGN KEY (evlStudentID)
REFERENCES Student(stdID)
ALTER TABLE Evaluation
ADD CONSTRAINT fk_EvlDisID
FOREIGN KEY (evlDisciplineSemesterID)
REFERENCES DisciplineSemester(disID)

ALTER TABLE [Group]
ADD CONSTRAINT fk_GrpSem
FOREIGN KEY (grpSemesterID)
REFERENCES Semester(semID)

ALTER TABLE ProposedDiscipline
ADD CONSTRAINT fk_PrdTchID
FOREIGN KEY (prdTeacherID)
REFERENCES Teacher(tchID)
ALTER TABLE ProposedDiscipline
ADD CONSTRAINT fk_PrdDisID
FOREIGN KEY (prdDisciplineSemesterID)
REFERENCES DisciplineSemester(disID)

ALTER TABLE SelectedDiscipline
ADD CONSTRAINT fk_SldStdID
FOREIGN KEY (sldStudentID)
REFERENCES Student(stdID)
ALTER TABLE SelectedDiscipline
ADD CONSTRAINT fk_SldDisID
FOREIGN KEY (sldDisciplineSemesterID)
REFERENCES DisciplineSemester(disID)

ALTER TABLE Semester
ADD CONSTRAINT fk_SemFacID
FOREIGN KEY (semFacultyID)
REFERENCES Faculty(facID)

ALTER TABLE Student
ADD CONSTRAINT fk_StdAccID
FOREIGN KEY (stdAccountID)
REFERENCES Account(accID)

ALTER TABLE StudentGroup
ADD CONSTRAINT fk_StgStdID
FOREIGN KEY (stgStudentID)
REFERENCES Student(stdID)
ALTER TABLE StudentGroup
ADD CONSTRAINT fk_StgGrpID
FOREIGN KEY (stgGroupID)
REFERENCES [Group](grpID)

ALTER TABLE Teacher
ADD CONSTRAINT fk_TchAccID
FOREIGN KEY (tchAccountID)
REFERENCES Account(accID)

ALTER TABLE TeacherDepartment
ADD CONSTRAINT fk_TdeTchID
FOREIGN KEY (tdeTeacherID)
REFERENCES Teacher(tchID)
ALTER TABLE TeacherDepartment
ADD CONSTRAINT fk_TdeDepID
FOREIGN KEY (tdeDepartmentID)
REFERENCES Department(depID)

ALTER TABLE AcceptedDiscipline
ADD CONSTRAINT fk_AcdDepID
FOREIGN KEY (acdDepartmentID)
REFERENCES Department(depID)
ALTER TABLE AcceptedDiscipline
ADD CONSTRAINT fk_AcdDisID
FOREIGN KEY (acdDisciplineSemesterID)
REFERENCES DisciplineSemester(disID)


/* Create indexes */
CREATE INDEX idxActFkTch
ON Activity (actTeacherID)
CREATE INDEX idxActFkDis
ON Activity (actDisciplineSemesterID)

CREATE INDEX idxDepFkTch
ON Department (depHeadID)
CREATE INDEX idxDepFkFac
ON Department (depFacultyID)

CREATE INDEX idxDisFkDsc
ON DisciplineSemester (disDisciplineID)
CREATE INDEX idxDisFkSem
ON DisciplineSemester (disSemesterID)

CREATE INDEX idxEvlFkStd
ON Evaluation (evlStudentID)
CREATE INDEX idEvlFkDis
ON Evaluation (evlDisciplineSemesterID)

CREATE INDEX idxGrpFkSem
ON [Group] (grpSemesterID)

CREATE INDEX idxPrdFkTch
ON ProposedDiscipline (prdTeacherID)
CREATE INDEX idxPrdFkDis
ON ProposedDiscipline (prdDisciplineSemesterID)

CREATE INDEX idxSldFkStd
ON SelectedDiscipline (sldStudentID)
CREATE INDEX idxSldFkDis
ON SelectedDiscipline (sldDisciplineSemesterID)

CREATE INDEX idxSemFkFac
ON Semester (semFacultyID)

CREATE INDEX idxStdFkAcc
ON Student (stdAccountID)

CREATE INDEX idxStgFkStd
ON StudentGroup (stgStudentID)
CREATE INDEX idxStgFkGrp
ON StudentGroup (stgGroupID)

CREATE INDEX idxTchFkAcc
ON Teacher (tchAccountID)

CREATE INDEX idxTdeFkTch
ON TeacherDepartment (tdeTeacherID)
CREATE INDEX idxTdeFkDep
ON TeacherDepartment (tdeDepartmentID)

CREATE INDEX idxAcdFkDep
ON AcceptedDiscipline (acdDepartmentID)
CREATE INDEX idxAcdFkDis
ON AcceptedDiscipline (acdDisciplineSemesterID)