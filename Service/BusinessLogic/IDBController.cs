﻿using System.Data;

namespace Service.BusinessLogic
{
    /// <summary>
    /// Description:
    ///     IDBController is the interface used by all Controller classes; it provides basic CRUD operations on a database
    ///     DataTable format:
    ///         - Add (example on Account):
    ///             data:
    ///                 HEADERS: TableName|AccountUsername|AccountPassword|AccountType|AccountName|AccountEmail|AccountApproved
    ///                 VALUES:  Account  |ExampleUsername|ExamplePassword|Student    |ExampleName|ExampleEmail|true
    ///         - Modify (example on Account):
    ///             data:
    ///                 HEADERS: TableName|AccountID|AccountUsername|AccountPassword|AccountType|AccountName|AccountEmail|AccountApproved
    ///                 VALUES:  Account  |GUID     |ExampleUsername|ExamplePassword|Student    |ExampleName|ExampleEmail|true
    ///         - Remove (example on Account):
    ///             data:
    ///                 HEADERS: TableName|AccountID
    ///                 VALUES:  Account  |GUID
    ///         - Filter (example on Account, Student, StudentGroup, searching for all Account fields and StudenID where 
    ///                   AccountName is ExampleName and the corresponding student has at least one signed contract, ordered by
    ///                   AccountUsername ascending and by AccountName descending):
    ///             tableNames
    ///             [the tables that will be used to find the data, including any intermediary tables]
    ///                 HEADERS: Account|Student|StudentGroup
    ///                 VALUES:         |       |
    ///             displayFields
    ///             [if you want all the fields of a table, you need to write the table's name, otherwise, write the desired fields]
    ///                 HEADERS: Account|StudentID
    ///                 VALUES:         |
    ///             filters
    ///                 HEADERS: AccountName|StudentGroupSigneContract
    ///                 VALUES:  ExampleName|true
    ///             orders
    ///                 HEADERS: AccountUsername|AccountName
    ///                 VALUES:  ASC            |DESC
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined IDBController interface with the following method(s): Add, Filter, Modify, Remove
    /// Modified by Andrei Popa on 06/04/2016
    ///     -Documented the interface
    /// </summary>
    interface IDBController
    {
        /// <summary>
        /// Description:
        ///		Adds a new entity in the repository.
        /// Parmeters:
        ///		[IN data: DataTable - a table containing the data to be added]
        /// Returns:
        ///		[void]
        /// </summary>
        void Add(DataTable data);

        /// <summary>
        /// Description:
        ///		Modifies an entity from the repository.
        /// Parmeters:
        ///		[IN data: DataTable - a table containing the data to be modified]
        /// Returns:
        ///		[void]
        /// </summary>
        void Modify(DataTable data);

        /// <summary>
        /// Description:
        ///		Removes an entity from the repository.
        /// Parmeters:
        ///		[IN data: DataTable - a table containing the data to be removed]
        /// Returns:
        ///		[void]
        /// </summary>
        void Remove(DataTable data);

        /// <summary>
        /// Description:
        ///		Returns filtered data from the database.
        /// Parmeters:
        ///		[IN tableNames: DataTable - the tables that will be used searchng for the data]
        ///     [IN displayFields: DataTable - the fields that will be brought from the repository]
        ///     [IN filters: DataTable - the filtered values]
        ///     [IN order: DataTable - the order in which the data will arranged]
        /// Returns:
        ///		[DataTable - the filtered data]
        /// </summary>
        DataTable Filter(DataTable tableNames, DataTable displayFields, DataTable filters, DataTable order);
    }
}
