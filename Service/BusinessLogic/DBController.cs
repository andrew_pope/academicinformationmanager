﻿using Service.DataAccess;
using Service.Functional;
using Service.Functional.Exceptions;
using Service.Functional.HashAlgorithms;
using Service.Functional.Mappings;
using Service.Functional.Query;
using System;
using System.Collections.Generic;
using System.Data;

namespace Service.BusinessLogic
{
    /// <summary>
    /// Description:
    ///     DBController is the Controller
    /// Created by Andrei Popa on 21/03/2016
    ///     -Defined and implemented DBController class with the following method(s): Add, Filter, Modify, Remove, GetExceptions, ClearExceptions
    /// </summary>
    public class DBController : IDBController
    {
        private IRepository m_repository;

        public DBController()
        {
            m_repository = new DBRepository();
        }

        public void Add(DataTable data)
        {
            ProcessInputData(data);
            m_repository.Create(data.Rows[0]);
        }

        public void Modify(DataTable data)
        {
            ProcessInputData(data);
            m_repository.Update(data.Rows[0]);
        }

        public void Remove(DataTable data)
        {
            ProcessInputData(data);
            m_repository.Delete(data.Rows[0]);
        }
        
        public DataTable Filter(DataTable tableNames, DataTable displayFields, DataTable filters, DataTable order)
        {
            Query query = new Query();
            try
            {
                foreach (DataColumn column in tableNames.Columns)
                {
                    query.Tables.Add(RuntimeConfiguration.TableFieldMappings[column.ColumnName].DBName);
                }
                foreach (TablesRelation relation in RuntimeConfiguration.Relations)
                {
                    if (query.Tables.Contains(relation.FirstTableDBName) && query.Tables.Contains(relation.SecondTableDBName))
                    {
                        query.RelationalClauses.Add(new QueryRelationalClause(relation.FirstFieldName, relation.SecondFieldName));
                    }
                }
                foreach (TableFieldsMapping mapping in RuntimeConfiguration.TableFieldMappings.Values)
                {
                    if (query.Tables.Contains(mapping.DBName))
                    {
                        if (displayFields.Columns.Contains(mapping.DisplayName))
                        {
                            displayFields.Columns.Remove(mapping.DisplayName);
                            foreach (FieldMapping fieldMapping in mapping.FieldMappings)
                                if (!fieldMapping.IsPrivate)
                                    query.DisplayedFields.Add(fieldMapping.DBName);
                        }
                        foreach (FieldMapping fieldMapping in mapping.FieldMappings)
                        {
                            if (displayFields.Columns.Contains(fieldMapping.DisplayName))
                            {
                                if (fieldMapping.IsPrivate)
                                    throw new BusinessLogicException(
                                        null,
                                        fieldMapping.DisplayName + " is a private field! It cannot be displayed!",
                                        "User tried to display " + fieldMapping.DisplayName + " field, which is private!");
                                query.DisplayedFields.Add(fieldMapping.DBName);
                                displayFields.Columns.Remove(fieldMapping.DisplayName);
                            }
                            if (filters.Columns.Contains(fieldMapping.DisplayName))
                            {
                                query.ConditionalClauses.Add(
                                    new QueryConditionalClause(fieldMapping, filters.Rows[0][fieldMapping.DisplayName], "="));
                                filters.Columns.Remove(fieldMapping.DisplayName);
                            }
                            if (order.Columns.Contains(fieldMapping.DisplayName))
                            {
                                string orderType = (string)order.Rows[0][fieldMapping.DisplayName];
                                if ("ASC".Equals(orderType) || "DESC".Equals(orderType))
                                    query.OrderFields.Add(new QueryOrderField(fieldMapping, orderType));
                                else
                                    throw new BusinessLogicException(
                                        null,
                                        orderType + " is not a valid order type!",
                                        "User tried to order by " + fieldMapping.DisplayName + " " + orderType);
                                order.Columns.Remove(fieldMapping.DisplayName);
                            }
                        }
                    }
                }
                foreach (DataColumn column in displayFields.Columns)
                    throw new BusinessLogicException(
                        null,
                        column.ColumnName + " is not a valid display field!",
                        "A user tried to display the following field: " + column.ColumnName);
                foreach (DataColumn column in filters.Columns)
                    throw new BusinessLogicException(
                        null,
                        column.ColumnName + " is not a valid filter field!",
                        "A user tried to filter with the following field: " + column.ColumnName);
                foreach (DataColumn column in order.Columns)
                    throw new BusinessLogicException(
                        null,
                        column.ColumnName + " is not a valid order field!",
                        "A user tried to order by the following field: " + column.ColumnName);
                return m_repository.Read(query);
            }
            catch (InvalidCastException)
            {
                throw new BusinessLogicException("There is a field with an invalid type!");
            }
            catch (KeyNotFoundException)
            {
                throw new BusinessLogicException("A given field name could not be found!");
            }
        }

        /// <summary>
        /// Description:
        ///		Processes the input data;
        /// Parameters:
        ///     [IN/OUT data: DataTable - the data that will be processed]
        /// Returns:
        ///		[void]
        /// </summary>
        private void ProcessInputData(DataTable data)
        {
            if (data.Columns.Contains("AccountPassword"))
            {
                data.Columns["AccountPassword"].ColumnName = "AccountValidationCode";
                data.Rows[0]["AccountValidationCode"] = 
                    HashAlgorithms.SHA256Hash(
                        (string)data.Rows[0]["AccountUsername"] + (string)data.Rows[0]["AccountValidationCode"]);
            }
        }
    }
}
