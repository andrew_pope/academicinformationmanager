﻿namespace Service.BusinessLogic
{
    /// <summary>
    /// Description:
    ///     IAIMController is the interface used by all AIMController classes; it provides custom methods for current project
    /// Created by Andrei Popa on 06/04/2016
    ///     -Defined empty IAIMController interface
    /// </summary>
    interface IAIMController : IDBController
    {
    }
}
