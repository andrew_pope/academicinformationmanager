﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace ConsoleServer
{
    /// <summary>
    /// Description:
    ///     Server is the class that reveals and hides the service defined in the Service library
    /// Created by Andrei Popa on 07/04/2016
    ///     -Defined the Server class with the following method(s): Main, Start
    /// </summary>
    class Server
    {
        /// <summary>
        /// Description:
        ///		The main entrypoint in the CnsoleServer application
        /// Parmeters:
        ///		[IN args: string[] - the arguments given through the command line]
        /// Returns:
        ///		[void]
        /// </summary>
        static void Main(string[] args)
        {
            Start();
            Console.WriteLine("Service is host at " + DateTime.Now.ToString());
            Console.WriteLine("Host is running... Press  key to stop");
            Console.ReadLine();
        }

        /// <summary>
        /// Description:
        ///		Starts the web service
        /// Parmeters:
        ///		[None]
        /// Returns:
        ///		[void]
        /// </summary>
        static void Start()
        {
            //Create a URI to serve as the base address
            Uri httpUrl = new Uri("http://localhost:80/AIMService");

            //Create ServiceHost
            ServiceHost host = new ServiceHost(typeof(Service.AIMService), httpUrl);

            //Add a service endpoint
            host.AddServiceEndpoint(typeof(Service.IAIMService), new WSHttpBinding(), "");

            //Enable metadata exchange
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            host.Description.Behaviors.Add(smb);

            //Start the Service
            host.Open();
        }
    }
}
